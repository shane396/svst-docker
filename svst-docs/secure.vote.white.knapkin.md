# Secure.Vote - WhiteKnapkin

The goal of secure.vote is to faciliate secret ballot, end-to-end verifiable, high capacity, 100% digital voting from devices such as smart phones.

## Request for expansion

If you'd like to see this whiteknapkin turn into a whitepaper, please raise an issue with details of how you'd like this expanded.

## Mad Features (The Best Features. YUGE Features)

* Secret Ballot 
* Efficient; only 2 signature verifications required per fully anonymised vote
* Duress Mode
* 100% open protocol and end-to-end verifyable
* Open source (but the pseudo-propriatary kind, not libre but liberal license for private individuals)
* Voting via smartphone (better secuirty model than web/pc/mac)
* Verification via smartphone (for small elections, like 15 million votes - here in Australia)
* Verifying nodes able to verify about 12 billion votes per day (on a 2014 MBP 15" retina, though 1e10 votes would be around a terabyte of data, excluding anonymisation)

## Data Structs

Multihash: a 34 byte struct with a version identifying the hash and 32 bytes for the hash (see IPFS, etc)

Vote: an N byte arbitrary data structure

Box: A collection of K votes with K signatures over all votes (this is just as anonymous as a normal ballot box)

Pallet: A collection of boxes, could easily be 10-100 MB large

Pallet Header: the header for a pallet, probably 100-200 bytes, contains links to other pallet headers and metadata (and the multihash of the pallet header)

Block: a Bitcoin block

## Prelude: Quanta

Quanta was the name of a type of blockchain I designed in 2014, see https://github.com/XertroV/quanta-test

We've adapted the idea to work on top of the Bitcoin blockchain (so a blockchain on a blockchain, if you will).

The initial selling point for Quanta was instant transactions on a PoW blockchain (yes really)

The basic idea:

* Every transaction is a block
* Blocks link to multiple previous blocks
* There's an algorithm for deterministic ordering of blocks based on 'weight' which was the 'deltawork' in this case (the amount of work added to the chain by that block)
* Normal txs would have a small deltawork
* However there would be txs with large deltawork that would help secure the chain
* Block reward was based on the deltawork

In SecureVote we're mostly just using multiple backlinks and deterministic ordering via a proof-of-stake esq method (where the stake is actually votes)

We're using this idea to build a scalability layer over Bitcoin - which will eventually be able to be used for arbitrary networks of smart contracts.
We actually use multiple DAGs, one for the whitelist of eligeable voters, one for each election, etc. That way they're 'disposable' - IE not needed for future elections.

My innovation was developing an algorithm to deterministically order nodes, essentially collapsing the dag into a chain.

Side note: Since all relevant quanta dags will be anchored to the Bitcoin blockchain they can safely talk to each other, and rely on each other for consensus, even though the only common history they share is via Bitcoin, not via either of their consensus protocols. This is relevant to my communication-across-chains hobby (see http://xk.io/2013/12/22/factum-exchange/ for example)

Because we can run many parallel chains, we can run a new chain for every new whitelist (i.e. set of eligible voters), and a new chain for every new democracy, or election, etc. These election chains would then refer back to the whitelist chain.

This also means chains are somewhat more 'disposable' -- running a full verifier for some election doesn't require 
validating any other elections, and at the end of an election we can just delete the data.

We call this sort of chain (or DAG) an 'open propriatary blockchain'. That is to say there is no privacy, but there's 
probably an authority deciding the rules. The idea here is that these chains are like individual smart contracts.

Because quanta DAGs are anchored to Bitcoin, they are able to determine state based on other quanta DAGs, so you can 
establish links between them (either one or two ways). Additionally, because such a link adopts the foreign chain as 
part of the consensus protocol it forms a much stricter network than the 'anyone to anyone' model of Ethereum et al.

Quanta dags are highly scalable, and are limited only by the complexity of the operations, the size of the data, and 
the length of time the chain exists for (they are intended to expire or remain sustainable). 100 GB in 1 day for a 
national election is completely plausible, for example, as the chain is killed (essentially) after the election is done. 
The 100GB can then be archived, and don't need to be used for any other purpose than just verifying the election in the future.

It is my conjecture that the majority of Bitcoin transactions will soon become anchored chains, which are just as 
flexible as side chains, but have the entire security of Bitcoin behind them. Many pallets, perhaps from different 
chains, can be combined via a merkle tree or in series, also, to futher reduce transaction burden. That will only 
happen once tx fee pressure forces them too, though (and 30 US cents is so cheap).

If we can put 100,000 votes in a bitcoin tx for 30 cents, we'll happily put 1,000 votes 100 times for 30 dollars 
too, since that's still a negligiable cost.

## Backbone - Blockchain

We use nulldata transactions (aka OP_RETURN txs) to store a 6 byte tag and the 34 byte multihash of a pallet header. 
Pallet headers (like pallets) are hosted via IPFS primarily, though any other protocol can be used also 
(like HTTP or swarm) provided the multihash is verified.

Pallet headers reference the multihash of a pallet, which is downloaded and verified by verifying all signatures.

Pallet headers also link together to form a quanta DAG (providing deterministic ordering) and ensures that even 
though data is primarily accessed through a DHT it will remain available. The DAG gives us a consensus mechanism 
that pretty much guarentees pallets will remain available.

### y u no use ethereum?!?!?!?!?!?

* NSA have lots of GPUs (only relevant till serenity ofc)
* PoS - still don't think it's more secure than PoW in the context of ethereum (our voting networks will be 
PoS effectively, but we can introduce a rule like 'no links more than 6 blocks behind' to the pallet headers 
and we essentially get the best of both worlds)
* We can always switch if Ethereum magically becomes more secure
* since we're talking 100s of gigs of data, Ethereum can't handle it directly 
(and why would we want random ethereum users to _have_ to verify our votes anyway)

and finally

Bitcoin works for us just as it is, we don't need bigger blocks or any of that. Ethereum still has a lot of work to 
do and we don't know what it'll look like going into the future, not nearly as much as we know what Bitcoin will look like, at least.

## Smart Phones & Apps

Phones can generate their own key or operate in a multisig fashion, initially we'll only support a single signing key, though.

There'll be an app, which does:

* key generation or interface with a hardware device
* voter verification and white list submission (basically same deal as a cert authority in terms of the architecture)
* actual voting (supporting IBDD / LD / DD / or regular elections, all voting methods that can be serialized are supported)
* verification of votes via SPV proofs
* non-cryptographic receipt (a crypto receipt would break secret ballot) - vote has a nonce attached, the location of the vote + the nonce combine to form the receipt

Pretty much all of these problems have been solved already, so deets not included here.

## Anonymous Voting

This method is covered by AU2017900257 (Patent Pending).

Hereon out we use the term oblivious shuffle to mean a process whereby the association between a public key 
(used for authentication) and data the actor would like included in a set (such as a vote or bitcoin output) is 
removed. That is to say: their contribution is anonymised.

Essentially we do two oblivious shuffles (using either the oblivious shuffle algorithm outlined in the CoinShuffle 
paper, or using a MixNet) - the first for an ephemeral voting pubkey, the second for the actual vote. The initial 
identity is a voter's Main Public Key which is on a whitelist maintained by the electoral authority.

By shuffling ephemeral voting keys first, then shuffling votes, we have the property that if an active attacker 
attempts to de-anonymise someone they can either obtain:

* A mapping of main public keys to ephemeral voting public keys (round 1)
* or, A mapping of ephermeal voting public keys to votes (round 2)

An active attack is not possible without alerting the voters involved in the shuffle, so if this occurs during 
round 1 the voters just generate new ephemeral voting keys, or if it happens in round 2 it does not de-anonymise 
the voter because they're already using an anonymised voting key.

If something goes wrong a 'blame game' is played, which is described in more detail in the CoinShuffle paper.

Some stats:

* Using the oblivious shuffle laid out in CoinShuffle is O(n) in time
* Using the mixnet laid out in the patent is O(1) - though might be slower than the oblivious shuffle under some conditions

## Duress Mode

So this mode isn't perfect but it's a good start.

It will work flawlessly in referendums (since it's a binary yes/no so votes are highly indistinguishable) but not with preferential voting and many many candidates (because you can hide unique messages in the least significant votes - note this breaks all forms of secret ballot, even in old school paper elections).

The idea is: instead of putting in your regular PIN (to decrypt your privkeys), you put in your duress PIN. You then pick the opposite of what you want (or prefill a ballot in private) and then the software gives you _someone else's receipt_. So instead of giving you your vote, it finds a vote that matched what you wanted to show, thus fooling your abusive husband, or shithead boss.

You can even create a market s.t. someone else who matches your chosen most significant votes takes your 'magic' least significant votes (intended to cause a signature able to be detected later) and uses them, thus generating an exact match. Obviously this is far more complex, but it illustrates that solutions to the 'being watched 50% of the time' problem exist. It does require some private time, if for no other reason than to set the duress PIN.

## End-to-end verification

Since all votes are public, the result of the election can be easily calculated by a full node. 

If keeping votes secret till the end of the election is desired then a multi-party ceremony can take place where keys are generated and PKs published to the voting system. Votes are then encrypted with these 1 or more keys, and at the end of the election these keys (the secret bits) are published on the blockchain and thus the result of the election becomes 100% transparent. Yay.

