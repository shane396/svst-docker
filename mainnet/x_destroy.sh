#!/usr/bin/env bash

function sshdown {
    ssh "$1@$2" "cd src/svst/svst-docker; ./mainnet/btc stop; ./mainnet/dc down --remove-orphans"
}

sshdown "$1" "$2"
