#!/bin/bash

# Test secret key
export PRODUCER=true
echo -n "Paste hex-encoded signing key > "
read -s privkey
export VERIFY_SECRET_KEY="$privkey"
echo ""
printf "%s is %d characters" "privkey" ${#privkey}
echo ""
./mainnet/r2_auditor.sh
