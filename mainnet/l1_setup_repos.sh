#!/usr/bin/env bash

function sshcmd {
    echo ""
    echo " # SSHING $2"
    echo ""
    ssh "$1@$2" "$3"
}

COMPOSE_URL="https://github.com/docker/compose/releases/download/1.11.2/docker-compose-Linux-x86_64"

MASSIVE_CMD="sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get install -y --no-install-recommends \
        linux-image-extra-\$(uname -r) \
        linux-image-extra-virtual \
        apt-transport-https \
        ca-certificates \
        curl htop \
        software-properties-common \
        && curl -fsSL https://apt.dockerproject.org/gpg | sudo apt-key add - && echo 'Docker apt key added' \
        && sudo add-apt-repository \"deb https://apt.dockerproject.org/repo/ ubuntu-\$(lsb_release -cs) main\" \
        && sudo apt-get update && sudo apt-get install -y docker-engine=1.13.1-0~ubuntu-xenial \
        && curl -L \"$COMPOSE_URL\" > docker-compose && sudo mv docker-compose /usr/local/bin/docker-compose \
        && sudo chmod +x /usr/local/bin/docker-compose && sudo gpasswd -a ubuntu docker"

sshcmd "$1" "$2" "$MASSIVE_CMD"
