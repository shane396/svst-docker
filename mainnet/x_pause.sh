#!/usr/bin/env bash

function sshcmd {
    ssh "$1@$2" "cd src/svst/svst-docker; ./mainnet/btc stop; ./mainnet/dc stop -t 20"
}

sshcmd "$1" "$2"
