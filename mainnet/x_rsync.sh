#!/usr/bin/env bash

function rsyncto {
    echo ""
    echo " # RSYNC-ing to $2"
    echo ""
    ssh "$1@$2" "mkdir -p ~/src/svst/svst-docker"
    rsync -az --progress --exclude=".git" --exclude="bitcoin-nulldata" --exclude="*.o" \
        --exclude=".stack-work" --exclude="*.pyc" --exclude="*.pallet" \
        . "$1@$2:src/svst/svst-docker/"
}

rsyncto "$1" "$2"
wait
