#!/usr/bin/env python3

import logging
import signal

logging.basicConfig(level=logging.INFO)

from tornado.platform.asyncio import AsyncIOMainLoop
import asyncio
AsyncIOMainLoop().install()

import socket
import time
import sys

import psycopg2
import aiopg
import tornado.ioloop
import tornado.web

from environment import dbhost, dbname, dbuser, is_producer, magic_bytes


if is_producer:
    logging.error("Exiting as this appears to be a PRODUCER; killing self to avoid computational overhead")
    sys.exit(0)
else:
    logging.info("Starting up, PRODUCER set to %s", is_producer)


PORT = 8683


while True:
    try:
        dbhost_ip = socket.gethostbyname(dbhost)
        sqlconn = aiopg.connect(database=dbname, host=dbhost_ip, user=dbuser)
    except (socket.gaierror) as e:
        logging.error("Unable to connect to Database, sleeping 1s: %s" % e)
        time.sleep(1)
    else:
        break


async def setup_db():
    global pool
    while True:
        try:
            pool = await aiopg.create_pool('dbname=%s host=%s user=%s' % (dbname, dbhost_ip, dbuser))
        except psycopg2.OperationalError as e:
            logging.info("Postgres not available, sleeping 5s")
            await asyncio.sleep(5)
        else:
            break


async def run_query(query, args=tuple()):
    global pool
    ret = []
    try:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query, args)
                async for row in cur:
                    ret.append(row)
    except Exception as e:
        logging.error("Unable to run query: %s", e)
        ret = [[]]
    rows = list([[a if type(a) is not memoryview else bytes(a) for a in row] for row in ret])
    return rows


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self, *args, **kwargs):
        self.set_header(r'Access-Control-Allow-Origin', r'*')
        self.set_header(r'Access-Control-Allow-Headers', r'Content-Type')

    def options(self, *args, **kwargs):
        pass


class RecentTxsHandler(BaseHandler):
    async def get(self, *args, **kwargs):
        txs = await run_query("SELECT tx_id FROM null_data WHERE prefix = %s ORDER BY id DESC LIMIT 50", (magic_bytes,))
        self.write({"txs": list([tx[0] for tx in txs])})


class GetInfoHandler(BaseHandler):
    async def get(self, *args, **kwargs):
        state = (await run_query("SELECT * FROM state_cache WHERE id=1"))[0]
        self.write(dict(zip(['id', 'last_processed', 'n_pallets', 'n_votes', 'votes_for', 'votes_against'], state)))


class GetVelocityHandler(BaseHandler):
    async def get(self, *args, **kwargs):
        fields = ['id', 'timestamp', 'n_votes', 'n_pallets', 'votes_for', 'votes_against']
        state_hist = await run_query("SELECT %s FROM state_log ORDER BY id DESC" % ', '.join(fields))
        if len(state_hist) == 0:
            state_hist = [[0, 0, 0, 0, 0, 0]]
        state_vs = [(state_hist[n][0], state_hist[n][1],
                     (state_hist[n][2] - state_hist[n-1][2]) / (state_hist[n][1] - state_hist[n-1][1]))
                    for n in range(1, len(state_hist))]
        self.write({"velocity_data": list([{'id': v[0], 'last_processed': v[1], 'state_vps': v[2],
                                            'header_vps': 0, 'pallet_vps': 0} for v in state_vs])})


class GetStateHistHandler(BaseHandler):
    async def get(self, *args, **kwargs):
        fields = ['id', 'timestamp', 'n_votes', 'n_pallets', 'votes_for', 'votes_against']
        state_hist = await run_query("SELECT %s FROM state_log ORDER BY id DESC" % ', '.join(fields))
        if len(state_hist) == 0:
            state_hist = [[0, 0, 0, 0, 0, 0]]
        self.write({"state_hist": list([dict(zip(fields, vc)) for vc in state_hist])})


def make_app():
    return tornado.web.Application([
        (r"/getinfo", GetInfoHandler),
        (r"/getvelocity", GetVelocityHandler),
        (r"/getstatehist", GetStateHistHandler),
        (r"/recenttxs", RecentTxsHandler),
        (r"/", tornado.web.RedirectHandler, {"url": "/index.html"}),
        (r"/(.*)", tornado.web.StaticFileHandler, {'path': './static/'}),
    ])


# https://gist.github.com/mywaiting/4643396
def sig_handler(sig, frame):
    logging.warning('Caught signal: %s', sig)
    asyncio.get_event_loop().run_until_complete(shutdown())


async def shutdown():
    global app
    logging.info('Stopping http server')
    app.stop()

    MAX_WAIT_SECONDS_BEFORE_SHUTDOWN = 2
    logging.info('Will shutdown in %s seconds ...', MAX_WAIT_SECONDS_BEFORE_SHUTDOWN)

    await asyncio.sleep(MAX_WAIT_SECONDS_BEFORE_SHUTDOWN)
    asyncio.get_event_loop().stop()
    logging.info('Shutdown')


app = make_app()
if __name__ == "__main__":
    app = make_app()
    app.listen(PORT, address='0.0.0.0')
    logging.info("Bringing vote explorer up on port %d" % PORT)
    asyncio.get_event_loop().run_until_complete(setup_db())
    logging.info("DB Connection set up")

    signal.signal(signal.SIGTERM, sig_handler)
    signal.signal(signal.SIGINT, sig_handler)

    asyncio.get_event_loop().run_forever()
