import subprocess
import logging


def sp_run(args):
    completed_result = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    logging.info(completed_result.stdout)
    return completed_result
