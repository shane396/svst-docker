import socket
import logging
import time

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException

from environment import rpcpassword, rpcuser


class RPCCall:
    def __init__(self, service_url, name):
        self.service_url = service_url
        self.name = name

    def __call__(self, *args):
        while True:
            try:
                return AuthServiceProxy(self.service_url, self.name)(*args)
            except Exception as e:
                logging.error("(sleeping 5) BitcoindWrapper encounted an error: %s", e)
                time.sleep(5)


class BitcoindWrapper:
    def __init__(self, service_url, *args, **kwargs):
        self.service_url = service_url

    def __getattr__(self, item):
        return RPCCall(self.service_url, item)


while True:  # continuously attempt to do this until they are up
    try:
        try:
            bitcoind = BitcoindWrapper("http://%s:%s@bitcoind:8332" % (rpcuser, rpcpassword))
            bitcoind.getinfo()  # try to connect in some way
        except socket.gaierror:  # Just DNS!
            logging.info("Trying localhost...")
            bitcoind = BitcoindWrapper("http://%s:%s@127.0.0.1:8332" % (rpcuser, rpcpassword))
            bitcoind.getinfo()
            logging.info("Bitcoin localhost connected...")

    except (socket.gaierror, JSONRPCException, ConnectionRefusedError) as e:
        logging.info("Sleeping 5; Unable to connect to Bitcoind: %s" % e)
        if "Transaction too large" in str(e):
            break
        time.sleep(5)
    else:
        break
