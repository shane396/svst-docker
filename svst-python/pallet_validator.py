#!/usr/bin/env python3

"""
This program is responsible for validating downloaded pallets.

* loop forever
* access DB to find all unvalidated/unprocessed pallets (but that have been downloaded) in pallet_validated
* for each pallet
  * download from IPFS to local DIR
  * run `validate-pallet` (the C exe) over the pallet
  * delete the pallet we downloaded from local DIR
  * if valid count votes
  * insert into DB pallet_processed with 0 for vote columns is pallet is invalid
"""

import logging
# Initialise logging and print a header
logging.basicConfig(level=logging.INFO)
logging.warning("~~~~~~~~\n\n### Pallet Validation Starting ###\n\n~~~~~~~~~")

import time
import subprocess
import os
import sys
import io
import contextlib
import multiprocessing.pool

from database import run_query, get_pallets_downloaded_needing_validation, add_to_pallet_validated, revalidate_pallet
from ipfs import ipfs, get_pin
from fancy_log import fancy_log
from graceful_shutdown import should_shutdown
from environment import pallet_period, is_producer

from svst.structs import unpack_and_count_pallet, PalletUnpackError


if is_producer:
    logging.error("Exiting as this appears to be a PRODUCER; killing self to avoid computational overhead")
    sys.exit(0)
else:
    logging.info("Starting up, PRODUCER set to %s", is_producer)


def validate_pallet(pallet_file):
    """ Validate pallets that have been downloaded """
    logging.info("Validating pallet file: %s", pallet_file)
    try:
        verify_proc = subprocess.run(["verify-pallet", pallet_file])
        validated = verify_proc.returncode == 0  # pallet is valid if `verify-pallet` returns 0
    except Exception as e:  # TODO: Figure out if there are any useful specific cases.
        logging.error("Running `verify-pallet` failed for %s raising %s", pallet_file, e)
        raise e
    return validated


def attempt_download_and_validation(pallet_mh):
    raw_pallet = get_pin(pallet_mh)
    if raw_pallet is None:  # not found
        return revalidate_pallet(pallet_mh)
    pallet_file = pallet_mh + ".pallet"
    valid = False
    try:
        with open(pallet_file, 'wb') as f:
            f.write(raw_pallet)
        logging.info("Downloaded pallet %s to %s.", pallet_mh, pallet_file)
        try:
            p_stats = unpack_and_count_pallet(io.BytesIO(raw_pallet))
            valid = validate_pallet(pallet_file)
            fancy_log("Sample Pallet results", p_stats)
            add_to_pallet_validated(pallet_mh, valid, p_stats.n_votes, p_stats.votes_for, p_stats.votes_against)
            logging.info("Added pallet %s to db with valid=%s", pallet_mh, valid)
        except PalletUnpackError as e:
            logging.error("Found bad pallet %s - threw PalletUnpackError %s", pallet_mh, e)
            add_to_pallet_validated(pallet_mh, False, 0, 0, 0)
    except Exception as e:
        logging.error("Found %s" % e)
        raise e
    finally:
        with contextlib.suppress(Exception):
            os.remove(pallet_file)


thread_pool = multiprocessing.pool.ThreadPool(4)


TICK_PERIOD = min(5, pallet_period)  # seconds
while not should_shutdown():
    # Get a list of pallets from the pallets_downloaded table:
    for row in get_pallets_downloaded_needing_validation():
        pallet_mh = row[0]
        # Try to download the pallet
        thread_pool.apply_async(attempt_download_and_validation, (pallet_mh,))
    time.sleep(TICK_PERIOD)
