var SCREEN_WIDTH = window.innerWidth,
    SCREEN_HEIGHT = window.innerHeight;

function tresComas() {
    var comas = []
    for (var i = 0; i < 3; i++) {
        var img = document.createElement("img");
        img.src = "coma.png";
        img.style.position = "absolute";
        img.style.left = SCREEN_WIDTH/2 - 215/2 + "px";
        img.style.top = SCREEN_HEIGHT/2 - 275/2 + "px";
        img.style["-webkit-transform"] = "translate(-50%, -50%)"; 
        document.getElementById("body").appendChild(img); 
        comas.push(img);       
    }
    var billion = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    TweenLite.from(comas, 2, {rotation:1080, scaleX:0, scaleY:0, ease:Back.easeOut.config(1)});

    TweenLite.to(comas[0], 1, {x:"-=300px", delay:1.5});
    TweenLite.to(comas[1], 1, {x:"+=300px", delay:1.5});
    TweenLite.to(comas, 1, {scaleX:0.2, scaleY:0.2, delay:3});
}

