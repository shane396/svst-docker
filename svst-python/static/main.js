//
// MAIN
//
var flux = this;
flux.plotData = [
    {x: [], y: [], type: 'bar', name: "State votes per Minute", dataName: "state_vps"}
];

flux.updateGraphs = function(data){
    var idArray = makeDataArray(data, "id").slice(0, 60);
    flux.plotData[0].x = idArray;
    flux.plotData[0].y = makeDataArray(data, flux.plotData[0].dataName).slice(0, 60);
    if (document.getElementById('voteVelocityGraph')) {
        Plotly.redraw('voteVelocityGraph');
    }
}

flux.generateGraphs = function(){
    var graphOptions = {
        title: 'Votes per Minute',
        margin:{l:40, r:10, b:20, t:40},
        width:400
    };
    Plotly.newPlot('voteVelocityGraph', flux.plotData, graphOptions);
}

flux.generateGraphs();
// tresComas();


// Modals
var modal = document.getElementById('myModal');
var array = ["flux", "xo1", "secure.vote", "faq", "vpm", "pallets"]
for (var i in array) {
    var btn = document.getElementById(array[i]);
    btn.onclick = function(event) {
        for (var j in array) { document.getElementById(array[j] + "-content").style.display = "none";}
        modal.style.display = "block";
        document.getElementById(event.target.id + "-content").style.display = "block";
    }
}
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}