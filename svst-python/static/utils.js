function sumArray(arr) {
  var total = 0;
  for(var i in arr) { total += arr[i]; }
  return total
}

function hslToRgb(h, s, l){
  var r, g, b;
  if(s == 0){
    r = g = b = l; // achromatic
  }else{
    var hue2rgb = function hue2rgb(p, q, t){
      if(t < 0) t += 1;
      if(t > 1) t -= 1;
      if(t < 1/6) return p + (q - p) * 6 * t;
      if(t < 1/2) return q;
      if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }
    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }
  var componentToHex = function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }

  var rgbToHex = function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
  }

  return rgbToHex(Math.round(r * 255), Math.round(g * 255), Math.round(b * 255));
}

function niceTime(time){
  var days = Math.floor(time/1000/60/60/24);
  var hours = Math.floor(time/1000/60/60) % 24;
  var minutes = Math.floor(time/1000/60) % 60;
  var seconds = Math.floor(time/1000) % 60;
  // var milliseconds = time % 1000;
  if (days < 10) { days = "0" + days; }
  if (hours < 10) { hours = "0" + hours; }
  if (minutes < 10) { minutes = "0" + minutes; }
  if (seconds < 10) { seconds = "0" + seconds; }
  return days + ':' + hours + ':' + minutes + ':' + seconds;
}

function makeDataArray(data, name){
  array = [];
  for (var i in data) {
    array.push(data[i][name]*60); //convert vps to vpm
  }
  return array;
}