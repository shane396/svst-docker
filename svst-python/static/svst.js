//
// UTILS
//


// http://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
function getParam(val) {
    var result = undefined,
        tmp = [];
    location.search
        //.replace ( "?", "" )
        // this is better, there might be a question mark inside
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
        });
    return result;
}


//
// DEBUG
//

var __debug__ = getParam('debug') !== undefined;  // just include ?debug=1 in the URL
var debug = {};
debug.scaling = 2.0;
var alertId = 0;

if (__debug__) {
    debug.getinfo = [];
    for(var _i = 0; _i < 144 * debug.scaling; _i++){
        var n_pallets = 70 * debug.scaling
        var votes_p_pallet = 150000 * debug.scaling
        debug.getinfo.push({
            'n_pallets': n_pallets * _i,
            'n_votes': n_pallets * _i * votes_p_pallet,
            'votes_for': n_pallets * _i * votes_p_pallet / 2,
            'votes_against': n_pallets * _i * votes_p_pallet / 2,
        })
    }
}

function generateDebugAlert() {
    console.log("Generating Alert");
    alertId++;
    return {
        "alert": true,
        "msg": "Test Alert" + alertId,
        "id": alertId
    }
}

//
// ANGULAR
//

var app = angular.module('svstApp', ['ngOdometer']);

app.controller('SvstController', function($scope, $rootScope, $log, $http, $window){
    $rootScope._ = _;
    $scope._ = _;
    var ctrl = this;
    ctrl.tab = 'intro';
    ctrl.log = [];  // log for msgs, {'type': 'error', 'msg': 'Test Error'}
    ctrl.tmp = {};
    ctrl.tres_comas = false;
    ctrl.testStarted = false;
    ctrl.startTime = 1488812400 * 1000;
    ctrl.vpm = 0;
    ctrl.showAlert;

    ctrl.setTab = function(newTab){ ctrl.tab = newTab; }
    ctrl.isTab = function(someTab){ return someTab == ctrl.tab; }

    ctrl.getinfo = {
        'n_pallets': 0,
        'n_votes': 0,
        'votes_for': 0,
        'votes_against': 0,
    }
    ctrl.getvelocity = {
        'state_log': [], 'pallet_log': [], 'header_log': []
    }
    ctrl.alert = {
        "alert": false,
        "msg": "",
        "id": 0
    }
    ctrl.previous_getinfo = ctrl.getinfo
    ctrl.previous_alert = ctrl.alert

    ctrl.errorHandler = function(err){
        ctrl.log.unshift({type: "error", msg: err, hide: false});
        $log.log("Error", err);
        $log.log(err);
    }
    ctrl.addMsg = function(msg){
        ctrl.log.unshift({type: "msg", msg: msg, hide: false})
        $log.log("added msg", msg);
    }

    ctrl.gotNewGetInfo = function(data){
        ctrl.previous_getinfo = ctrl.getinfo;
        ctrl.getinfo = data.data;
        console.log("Loaded data: ", data);
        if (!ctrl.tres_comas && data.data.n_votes > 1000000000) {
            console.log("tres comas");
            ctrl.tres_comas = true
            var launchInterval = setInterval(launch, 800);
            var loopInterval = setInterval(loop, 1000 / 50);
            setTimeout(function() {
                clearInterval(launchInterval);
                colourSpeed = 1;
            }, 5*60*1000);
        }
    }

    ctrl.gotNewGetVelocity = function(data){
        ctrl.previous_getvelocity = ctrl.getvelocity;
        ctrl.getvelocity = data.data.velocity_data;
        ctrl.vpm = Math.round(ctrl.getvelocity[0].state_vps)*60;
        console.log("Loaded data: ", data);
        flux.updateGraphs(data.data.velocity_data);
    }

    ctrl.gotNewAlert = function(data){
        ctrl.previous_alert = ctrl.alert;
        ctrl.alert = data.data;
        if (ctrl.previous_alert.id != ctrl.alert.id) {
            ctrl.showAlert = true;
        }
        console.log("Loaded data: ", data);
    }

    ctrl.gotRecentTxs = function(data){
        ctrl.prev_txs = ctrl.txs;
        ctrl.txs = data.data.txs;
        console.log("Loaded data: ", data);
    }

    ctrl.startingIn = function(){
        var countdown = ctrl.startTime - new Date().getTime();
        if (countdown <= 0) {
            ctrl.testStarted = true;
        }
        return niceTime(countdown);
    }

    ctrl.timeRunning = function(){
        return niceTime(new Date().getTime() - ctrl.startTime);
    }

    ctrl.loadData = function(){
        if (__debug__){
            ctrl.addMsg("New debug getinfo " + (Date.now() / 1000).toString())
            ctrl.gotNewGetInfo({data: debug.getinfo.shift()})
            if(__debug__ && Math.random()<0.5){ctrl.gotNewAlert({data: generateDebugAlert()})}
        } else {
            $http.get('/getinfo').then(ctrl.gotNewGetInfo, ctrl.errorHandler);
            $http.get('/getvelocity').then(ctrl.gotNewGetVelocity, ctrl.errorHandler);
            $http.get('/recenttxs').then(ctrl.gotRecentTxs, ctrl.errorHandler);
            $http.get('https://xo1.io/stress-test-alert.json').then(ctrl.gotNewAlert, ctrl.errorHandler);
        }
        setTimeout(ctrl.loadData, 3000);
        $scope.$apply();
    }
    setTimeout(ctrl.loadData, 1000);
});
