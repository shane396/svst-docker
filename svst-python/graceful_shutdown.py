import signal


shutdown = False


def exit_graceful(*args):
    global shutdown
    shutdown = True
signal.signal(signal.SIGINT, exit_graceful)
signal.signal(signal.SIGTERM, exit_graceful)


def should_shutdown():
    global shutdown
    return shutdown
