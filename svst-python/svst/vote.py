import os

from svst.structs import *


def create_vote(direction):
    return {
        'direction': direction,
        'nonce': os.urandom(3)
    }