import argparse
from io import BytesIO
import os, time

from svst.structs import *
from svst.box import create_box


parser = argparse.ArgumentParser()
parser.add_argument('--n-boxes', help='How many boxes in the pallet?', type=int, default=10000)
parser.add_argument('--no-header', action='store_true', default=True)
args = parser.parse_args()


def create_pallet(n_boxes=10000):
    return {'boxes': [create_box(64) for _ in range(n_boxes)]}


out = BytesIO()
pallet = Pallet.build(create_pallet(args.n_boxes))

with open('%s.pallet' % time.time(), 'wb') as f:
    f.write(pallet)
