import argparse
from io import BytesIO
import os, time

from svst.structs import *
from svst.box import create_box
from svst.pallet import create_pallet


parser = argparse.ArgumentParser()
parser.add_argument('--n-boxes', help='How many boxes in the pallet?', type=int, default=10000)
parser.add_argument('--no-header', action='store_true', default=True)
args = parser.parse_args()

pallet = Pallet.build(create_pallet(args.n_boxes))

with open('%s.pallet' % str(time.time())[:14], 'wb') as f:
    f.write(pallet)
