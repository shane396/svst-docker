import logging


class ProcessError(Exception):
    pass


def raise_process_error(completed_result):
    logging.error("Process stdout:")
    logging.error(completed_result.stdout)
    logging.error("Process stderr:")
    logging.error(completed_result.stderr)
    raise ProcessError('Process returned nonzero code: %s' % str(completed_result.returncode))
