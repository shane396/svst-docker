import logging
from pprint import pformat


def fancy_log(title, thing):
    logging.info("")
    logging.info("########" * 2 + "#" * len(title))
    logging.info("####### %s #######" % title)
    logging.info("########" * 2 + "#" * len(title))
    logging.info("")
    logging.info(pformat(thing, indent=2))
    logging.info("")
