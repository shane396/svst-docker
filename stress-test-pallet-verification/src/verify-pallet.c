/*
 * Author: Sean Seefried
 * Date:   Mon 28 Nov 2016
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>


#include "ed25519.h"
#include "test-ticks.h"
#include "cryptonite_sha3.h"

#include <pthread.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h> // for htonl function

#define THREADS 4  // TODO: Think about upping this - will it affect system responsiveness?

#define MAX_BATCH_COUNT 64 // can be between 1 and 64
#define WORD32_SIZE     4
#define VOTE_SIZE       5
#define SIG_SIZE        64
#define PK_SIZE         32
#define SIG_PAIR_SIZE   (PK_SIZE + SIG_SIZE)
#define SHA3_256_SIZE   32

  /*
   * Data structures (defined using Python Contruct's syntax)
   * http://construct.readthedocs.io/en/latest/
   *
   * Signature = Byte[64]
   * PubKey = Byte[32]
   * MultiHash = Byte[34]
   *
   * Vote = Struct(
   *     "direction" / Int8sb,
   *     "nonce"     / Byte[3]
   * )
   *
   *
   * SigPair = Struct(
   *     "signature" / Signature,
   *     "pub_key"   / PubKey
   * )
   *
   *
   * BallotBox = Struct(
   *     "count"    / Rebuild(Int32ub, len_(this.votes)),
   *     "votes"    / Vote[:],
   *     "sigpairs" / SigPair[:],
   *     Check(lambda ctx: len(ctx.votes) == len(ctx.sigpairs))
   * )
   *
   *
   * Pallet = Struct(
   *     "count" / Rebuild(Int32ub, len_(this.boxes)),
   *     "boxes" / BallotBox[:],  # using Int32ub means max Pallet length is 2^32 bytes => 4.29 GB
   * )
   *
   *
   * PalletHeader = Struct(
   *     "magic"       / Const(b"\x00"*4),
   *     "pallet_hash" / MultiHash,
   *     "auth_sig"    / Signature,  # From flux central command
   * )
   */

typedef struct {
  int              thread_id;       // doesn't change
  unsigned char   *pallet;          // mmap`ed pallet. Doesn't change
  unsigned char  **next_box;        // next box. Shared. Changes
  uint32_t         thread_box_num;  // not shared. changes
  uint32_t        *next_box_num;    // shared. changes
  uint32_t         current_num_votes; // not shared. changes
  int64_t         *total_ticks;     // shared. changes
  pthread_mutex_t *mutex;           // shared mutex. Doesn't change
  size_t          *message_lengths; // shared. doesn't change
  uint32_t         num_boxes;       // not shared. doesn't change
  uint64_t        *num_sigs;        // shared. changes
} verify_args ;


void printHex(unsigned char *s, size_t size) {
  int i;
  for (i = 0; i < size; i++) {
    printf("%02x", s[i]);
  }
  printf("\n");
}

static inline uint32_t box_votes(unsigned char *box) {
  return ntohl(*((uint32_t *) box));
}

/* Calculates box size (in bytes) given a box pointer, including the
 * vote count at the beginning
 */
static inline uint32_t box_size(unsigned char *box) {
  return WORD32_SIZE + box_votes(box) * (VOTE_SIZE + SIG_PAIR_SIZE);
}


static inline void hash_votes(unsigned char *vote_hash, unsigned char *votes,
                uint32_t num_votes) {
  int i;
  struct sha3_ctx *ctx = malloc(SHA3_CTX_256_MALLOC_SIZE);
  cryptonite_sha3_init(ctx, 256);
  cryptonite_sha3_update(ctx, (uint8_t *) votes, VOTE_SIZE*num_votes);
  cryptonite_sha3_finalize(ctx, 256, vote_hash);
  free(ctx);
}


/*
 * Reads a box from FILE pointer fp and correctly sets
 * the values of message_pointers, pk_pointers, sig_pointers
 *
 */
static inline int read_batch(verify_args *va,
                unsigned char *vote_hash,
                unsigned char *message_pointers[],
                unsigned char *pk_pointers[],
                unsigned char *sig_pointers[]) {
  int retval = -1;
  if ( !pthread_mutex_lock(va->mutex) ) {
    if (*va->next_box_num < va->num_boxes) {
      int i;
      unsigned char *sig_pair_base, *box;
      // This only works because all the votes are at the beginning of the
      // ballot box
      uint32_t num_votes = box_votes(*va->next_box);
      unsigned char *vote_base = *va->next_box + WORD32_SIZE;
      hash_votes(vote_hash, vote_base, num_votes);
      sig_pair_base = (unsigned char *) vote_base + num_votes*VOTE_SIZE;
      for (i = 0; i < num_votes; i++) {
        message_pointers[i] = vote_hash;
        sig_pointers[i]     = sig_pair_base + i*SIG_PAIR_SIZE;
        pk_pointers[i]      = sig_pair_base + i*SIG_PAIR_SIZE + SIG_SIZE;
      }

      va->thread_box_num     = *va->next_box_num;
      va->current_num_votes  = num_votes;
      *va->next_box         += box_size(*va->next_box);
      (*va->num_sigs)       += va->current_num_votes;
      (*va->next_box_num)++; // update shared box count


      retval = 0;
    } else {
      retval = -1;
    }
    // in either case release the mutex
    if (pthread_mutex_unlock(va->mutex) ) {
      printf("Mutex is invalid or current thread does not have lock!\n");
      exit(1);
    }

    return retval;
  } else {
    printf("Could not acquire mutex\n");
    exit(1);
  }
}

/* Precondition: Mutex must be initialised */
static inline void *verify_func(void *arg) {
  int i;
  verify_args *va = (verify_args *) arg;
  int *valid;
  uint64_t t;

  unsigned char        *vote_hash;
  const unsigned char **message_pointers;
  const unsigned char **pk_pointers;
  const unsigned char **sig_pointers;

  valid            = malloc(sizeof(int) * MAX_BATCH_COUNT);
  vote_hash        = malloc(SHA3_256_SIZE);
  message_pointers = malloc(sizeof(unsigned char *)*MAX_BATCH_COUNT);
  pk_pointers      = malloc(sizeof(unsigned char *)*MAX_BATCH_COUNT);
  sig_pointers     = malloc(sizeof(unsigned char *)*MAX_BATCH_COUNT);

  for (;;) {
    if ( read_batch(va, vote_hash, message_pointers, pk_pointers,
                    sig_pointers) == 0 ) {
      t = get_ticks();
      ed25519_sign_open_batch(message_pointers, va->message_lengths,
                              pk_pointers, sig_pointers, va->current_num_votes,
                              valid);
      // for (i =0; i < BATCH_COUNT; i++) { valid[i] = 1; }
      *va->total_ticks += (get_ticks() - t);
      // check validity
      for (i = 0; i < va->current_num_votes; i++) {
        if (valid[i] != 1) {
          printf("Invalid sig! thread=%d sig=%d, batch=%d\n", va->thread_id,
                 i, va->thread_box_num);
          printf("msg = "); printHex(message_pointers[i], SHA3_256_SIZE);
          printf("sig = "); printHex(sig_pointers[i], SIG_SIZE);
          printf("pk  = "); printHex(pk_pointers[i], PK_SIZE);
          exit(1);
        }
      }
    } else {
      break; // quit infinite loop
    }
  }
  free(valid);
  free(vote_hash);
  free(message_pointers);
  free(pk_pointers);
  free(sig_pointers);
  return NULL;
}

void init_mutex(pthread_mutex_t *mutex) {
  int err;
  if ( (err = pthread_mutex_init(mutex, NULL)) ) {
    switch (err) {
      case EAGAIN:
        printf("System lacks resources\n");
        break;;
      case EINVAL:
        printf("attribute invalid\n");
      case ENOMEM:
        printf("Cannot allocate enough memory\n");
    }
    exit(1);
  }
}

void init_shared_variables(int64_t *total_ticks, uint32_t *next_box_num,
                           size_t *message_lengths) {

  int i;
  for (i = 0; i < MAX_BATCH_COUNT; i++) {
    message_lengths[i] = SHA3_256_SIZE;
  }

  *total_ticks = 0;
  *next_box_num = 0;

}

int main(int argc, char **argv) {
  pthread_t threads[THREADS];
  struct           timeval start, finish;
  int              i, fd;
  unsigned char   *pallet;
  int              next_box_num;
  int64_t          total_ticks;
  pthread_mutex_t  mutex;
  size_t           message_lengths[MAX_BATCH_COUNT];
  struct           stat stat_buf;
  uint32_t         num_boxes;
  uint64_t         num_sigs = 0;
  int              pallet_size;
  verify_args     *vas[THREADS];
  unsigned char   *next_box;

  if (argc < 2) {
    printf("Usage: %s <pallet file>\n", argv[0]);
    exit(1);
  }

  init_mutex(&mutex);
  init_shared_variables(&total_ticks, &next_box_num, message_lengths);

  if ((fd = open(argv[1], O_RDONLY)) == -1) {
    printf("Error opening file '%s'\n", argv[1]);
    exit(1);
  }

  fstat(fd, &stat_buf);
  pallet_size = stat_buf.st_size;

  if ( (pallet = mmap(0, pallet_size, PROT_READ,
                       MAP_SHARED, fd, 0)) == MAP_FAILED ) {
    printf("error memory mapping file\n");
    exit(1);
  }

  num_boxes = ntohl(*((uint32_t *) pallet));
  printf("Verifying:  %d boxes\n", num_boxes);

  gettimeofday(&start, NULL);
  next_box = pallet + WORD32_SIZE;

  for (i = 0; i < THREADS; i++) {
    vas[i]                  = malloc(sizeof(verify_args));
    vas[i]->thread_id       = i;
    vas[i]->pallet          = pallet;
    vas[i]->next_box        = &next_box;
    vas[i]->total_ticks     = &total_ticks;
    vas[i]->mutex           = &mutex;
    vas[i]->next_box_num    = &next_box_num;
    vas[i]->message_lengths = message_lengths;
    vas[i]->num_boxes       = num_boxes;
    vas[i]->num_sigs        = &num_sigs;
    pthread_create(&threads[i], NULL, verify_func, (void *) vas[i]);
  }

  for (i = 0; i < THREADS; i++) {
    pthread_join(threads[i], NULL);
  }

  munmap(pallet, pallet_size);

  gettimeofday(&finish, NULL);
  double duration = (finish.tv_sec - start.tv_sec) +
                    (finish.tv_usec - start.tv_usec)/1e6;

  double sigs_per_sec = num_sigs/duration;

  printf("Duration:   %g s\n", duration);
  printf("Total sigs: %llu\n", num_sigs);
  printf("Sigs/s:     %.2f\n",   sigs_per_sec);
  printf("Ticks/sig:  %.2f\n", ((double) total_ticks)/((double) num_sigs));
  printf("Tres comas in %.2f hours\n", (1000000000/sigs_per_sec) / 3600);

  return 0;
}
