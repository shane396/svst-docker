#!/bin/bash

echo "RSYNC-ing"
rsync -az --progress --exclude=".git" --exclude="bitcoin-nulldata" --exclude="*.o" \
    --exclude=".stack-work" --exclude="*.pyc" \
    . root@138.68.51.172:src/svst/svst-docker/
