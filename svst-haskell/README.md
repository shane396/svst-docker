# SVST Null Data Scraper

## Creating database

    $ scripts/create-nulldata-db.sh

## Test flux signing key

The flux signing key used for the test (null data prefix of "TestFluxST") is

c005f96c358e44b45ed9145aa29d4af1ae9530c7bd79d582685c4a8f25fa7643
