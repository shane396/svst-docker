{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module SVST.Util where

import           Control.Concurrent
import           Control.Exception
import           Control.Monad
import           Data.Text (Text)
import qualified Data.Text.Lazy.IO as LT
import           Data.Time
import           Formatting
import           System.Exit
import           System.IO

retryDelaySecs :: Int
retryDelaySecs = 5

--
-- Retrieves all of the available input from a handle and writes it to
-- another handle
--
hRedirectAvailable :: Handle -> Handle -> IO ()
hRedirectAvailable fromH toH = catch go (\(_ :: SomeException) -> return ())
  where
    go = do
      b <- hReady fromH
      when b $ do
        c <- hGetChar fromH
        hPutChar toH c
        go

loop :: IO () -> IO ()
loop io = io >> loop io

loopWithMillisecondDelay :: Int -> IO () -> IO ()
loopWithMillisecondDelay ms io =
  io >> threadDelayMillisecs ms >> loopWithMillisecondDelay ms io

retryForever :: IO (Either Text a) -> IO a
retryForever io = do
  res <- io
  case res of
    Right a -> return a
    Left err -> do
      LT.putStrLn $ format ("ERROR: " % stext % "\n  Retrying in " %
                           int % " seconds") err retryDelaySecs
      threadDelaySecs retryDelaySecs
      retryForever io

-- threadDelaySecs delays for seconds instead of microseconds
threadDelaySecs :: Int -> IO ()
threadDelaySecs n = threadDelay (n*1000000)

threadDelayMillisecs :: Int -> IO ()
threadDelayMillisecs n = threadDelay (n*1000)

exitWithError msg = do
  putStrLn msg
  exitWith (ExitFailure 1)

isoTime :: UTCTime -> String
isoTime t = formatTime defaultTimeLocale "%F %T" t