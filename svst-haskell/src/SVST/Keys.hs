{-# LANGUAGE OverloadedStrings #-}
module SVST.Keys where

import Data.Maybe
import Data.Hex
import Crypto.Ed25519

-- This is public key for test signing key (null-data prefix of "TestFluxST")
fluxPublicKey :: PublicKey
fluxPublicKey = fromJust $ importPublic bytes
  where
    bytes = fromJust $ unhex
              "c9df7bcba2238bedcc681e8b17bb21c1625d21d285b70c20cf53fdd473db9dfb"