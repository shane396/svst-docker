{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module SVST.DBMonad where


import           Control.Monad.IO.Class  (liftIO)
import           Control.Monad.Trans.Reader (ReaderT(..))
import           Control.Monad.Trans.Resource
import           Control.Monad.Logger
import           Database.Persist.Sql
import           Database.Persist.Postgresql

type DBM a = ReaderT SqlBackend (NoLoggingT (ResourceT IO)) a

--
-- change loggerT below to, say, runStderrLoggingT when debugging
--
withDBM :: DBM b -> IO b
withDBM dbm =
  runLoggerT $ withPostgresqlConn connString $
    \(backend :: SqlBackend) -> liftIO $ flip runSqlPersistM backend $ dbm
  where
    connString = "host=postgres user=postgres dbname=postgres"
    runLoggerT = runNoLoggingT
