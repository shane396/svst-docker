{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
module SVST.NullData
where

import           Control.Exception
import           Control.Lens
import           Data.Aeson
import           Data.Aeson.Lens
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LB
import qualified Data.HashMap.Strict as H
import           Data.Int
import           Data.Text (Text)
import qualified Data.Text.Encoding as T
import           Data.Vector (Vector)
import qualified Data.Vector as V
import           Database.Persist
import           Data.Maybe
-- import           Formatting
import           Network.Wreq

-- friends
import           SVST.DBMonad
import           SVST.Persist
import           SVST.PersistTypes

-- FIXME: Get credentials from environment variables
rpcUsername, rpcPassword :: ByteString
rpcUsername = "bitcoinrpc"
rpcPassword = "YwdRRcTVR7auH0xWukWUd5qk77t2rxP6FKJlk1YL0YIM"

defaultNulldataPrefix :: ByteString
defaultNulldataPrefix  = "FluxST"

--------------------------------------------------------------------------------

type BlockIndex = Int32

bitcoinRpcJSON :: Text -> Vector Value -> Value
bitcoinRpcJSON method params =
  Object $
   H.fromList [ ("jsonrpc", String "2.0")
              , ("id",      String "nulldata-request")
              , ("method",  String method)
              , ("params",  Array params ) ]

data BlockRange = BlockRange BlockIndex BlockIndex
    deriving (Show, Eq)

type BlockTime = Int32
type BlockHash = SHA256

data BlockData = BlockData BlockIndex BlockTime BlockHash
    deriving (Show, Eq)

bitcoinRpc :: (ToJSON v, FromJSON v) => Text -> Vector Value
           -> IO (Either Text v)
bitcoinRpc method params = do
  let opts = defaults & auth ?~ basicAuth rpcUsername rpcPassword

  mbRes <- (Just <$> (postWith opts "http://bitcoind:8332" $
                      bitcoinRpcJSON method params))
           `catch` handleError
  return $ case mbRes of
    Just res -> do
      let mbResult = res ^? responseBody . key "result" . _JSON
      case mbResult of
        Nothing -> Left "bitcoind appears to be down or is taking a while to return results"
        Just r -> Right r
    Nothing -> Left $ "bitcoind RPC on port 8332 not open"
  where
    handleError :: SomeException -> IO (Maybe (Response LB.ByteString))
    handleError _ = return Nothing

--
-- The True parameter is so that only blocks with non-empty null datas will
-- be returned
--
getManyNullDatas :: BlockRange -> IO (Either Text [NullDataBlock])
getManyNullDatas (BlockRange from to) =
  bitcoinRpc "getmanynulldatas" (V.fromList [ n from, n to, Bool True])
  where
    n = Number . fromIntegral

getBlockHeight :: IO (Either Text BlockIndex)
getBlockHeight = do
  (etO :: Either Text Value) <- bitcoinRpc "getinfo" V.empty
  return $ do -- Either monad
    o <- etO
    let mbRes = round <$> (o ^? key "blocks" . _Number)
    case mbRes of
      Nothing -> Left $ "bitcoind 'getinfo' did not return object with 'blocks' field"
      Just res -> Right $ res

-----------

data NullDataBlock =
  NullDataBlock {
    ndBlockHash     :: Text
  , ndPrevBlockHash :: Text
  , ndBlockHeight   :: BlockIndex
  , ndBlockTime     :: BlockTime
  , ndTxs           :: [NullDataTx]
  }
  deriving (Read, Show, Eq)

data NullDataTx =
  NullDataTx {
    ndTxId      :: Text
  , ndNullDatas :: [Text]
  } deriving (Read, Show, Eq)


ndbToBlockData :: NullDataBlock -> BlockData
ndbToBlockData ndb = BlockData (ndBlockHeight ndb) (ndBlockTime ndb)
    (fromJust $ sha256FromHex $ T.encodeUtf8 $ ndBlockHash ndb)


instance FromJSON NullDataBlock where
  parseJSON (Object o) =
        NullDataBlock
    <$> o .: "hash"
    <*> o .: "previousblockhash"
    <*> o .: "height"
    <*> o .: "time"
    <*> o .: "nulldatatxs"
  parseJSON _ = fail "nulldata block must be a JSON object"

instance FromJSON NullDataTx where
  parseJSON (Object o) =
        NullDataTx
    <$> o .: "txid"
    <*> o .: "nulldatas"
  parseJSON _ = fail "nulldata tx must be a JSON object"


instance ToJSON NullDataBlock where
  toJSON _ = error "No need to toJSON to be defined for NullDataBlock"

----

toNullDatas :: [NullDataBlock] -> [NullData]
toNullDatas = concatMap nullDataBlockToNullDatas

nullDataBlockToNullDatas :: NullDataBlock -> [NullData]
nullDataBlockToNullDatas ndb =
  concatMap (nullDataTxToNullDatas bd) (ndTxs ndb)
  where
    bd = ndbToBlockData ndb

nullDataTxToNullDatas :: BlockData -> NullDataTx -> [NullData]
nullDataTxToNullDatas (BlockData ht btime bhash) ndt =
  map toNullData (ndNullDatas ndt)
  where
    txIdHex = ndTxId ndt
    toNullData nullDataHex =
      NullData (fromJust $ prefix6FromHex $ (BS.take 12) $ T.encodeUtf8 $ nullDataHex)
               (fromJust $ sha256FromHex $ T.encodeUtf8 $ txIdHex)
               (fromJust $ hex160FromHex $ T.encodeUtf8 $ nullDataHex) ht bhash

scrapeNullDatas :: BlockRange -> IO (Either Text ())
scrapeNullDatas br = do
  etNullDataBlocks <- getManyNullDatas br
  let etNullDatas :: Either Text [NullData] = do -- Either monad
        nullDataBlocks <- etNullDataBlocks
        return $ toNullDatas nullDataBlocks
  -- putStrLn ("Nulldatas for range " ++ (show br) ++ "are" ++ (show etNullDatas))
  case etNullDatas of
    Right nullDatas -> do
      putStrLn ("Found nulldatas: " ++ show nullDatas)
      withDBM $ mapM_ insertUnique nullDatas
      return $ Right ()
    Left err -> do
      putStrLn $ show err
      return $ Left err
