{-# LANGUAGE OverloadedStrings #-}
module SVST.Pallet  (
  -- types
  PalletHeaderError(..)
  -- functions
  , generatePallet
  , mkPalletHeader
  , palletHeaderToBase58Multihash
  , base58MultihashToByteString
  , byteStringToBase58Multihash
)


where


import           Control.DeepSeq
import           Control.Monad
import           Crypto.Ed25519.Pure
import           Crypto.Hash
import           Crypto.Random
import           Data.Binary.Put
import           Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as LB
import           Data.ByteString.Base58
import qualified Data.ByteArray as BA
import           Data.Maybe
import           Data.Time.Clock.POSIX
import           Data.Word
import           Data.Bits (xor, shiftR)
import           System.CPUTime
import qualified System.Random as UR  -- UR => UnsafeRandom

-- friends
import SVST.Keys

batchSize :: Word32
batchSize = 64

palletHeaderMagic :: ByteString
palletHeaderMagic = "FluxST"

palletHeaderVersion :: ByteString
palletHeaderVersion = "\00\00\00\00"  -- version is 0 to start with

-- For SHA2-256 multihash
multihashLen :: Int
multihashLen = 34

data SigPair = SigPair Signature PublicKey
    deriving (Show)

-- Sigpair is 32 + 64 = 96 bytes
szSigPair :: SigPair -> ByteString
szSigPair (SigPair (Sig sig) pk) = bs
  where
    bs = B.concat [ sig, exportPublic pk]


data Vote =
  Vote {
    _vMark  :: Word8
  , _vNonce :: Word32
  } deriving (Read, Show, Eq)

bsConcatMap :: (a -> ByteString) -> [a] -> ByteString
bsConcatMap f vs = B.concat $ map f vs

-- Vote is 5 bytes
szVote :: Vote -> ByteString
szVote (Vote d n) =
  B.concat [ B.singleton d, word32ToBigEndianByteString n ]

word32ToBigEndianByteString :: Word32 -> ByteString
word32ToBigEndianByteString = LB.toStrict . runPut . putWord32be

szVotes :: [Vote] -> ByteString
szVotes = bsConcatMap szVote


-- 0 <= num votes <= 64
data Box = Box Word32 {-num votes-} [Vote] [SigPair]
  deriving (Show)


-- Box is 64 * (sizeof(Vote) + sizeof(SigPair)) = 64 * (5 + 96) = 6464 bytes
szBox :: Box -> ByteString
szBox (Box n vs sps) = B.concat [ word32ToBigEndianByteString n,
                                  szVotes vs, bsConcatMap szSigPair sps]

-- Pallet is 10000 * sizeof(Box) = 64,640,000 bytes
data Pallet = Pallet Word32 [Box]
  deriving (Show)

szPallet :: Pallet -> ByteString
szPallet (Pallet n bs) = B.concat $ word32ToBigEndianByteString n:map szBox bs

generatePallet :: Word32 -> IO ()
generatePallet nBoxes = do
    putStrLn "Generating pallet..."
    pl <- mkPallet nBoxes
    let nSigs = batchSize * nBoxes
    putStrLn $ show nSigs ++ " signatures being generated"
    start <- getCPUTime
    end <- szPallet pl `deepseq` getCPUTime
    let duration = (end - start)  -- in picoseconds, divide by 10^12 later
    let tres_comas = 10^9 * fromIntegral duration / fromIntegral nSigs / 60 / 60 / 10^12
    putStrLn ("Generating signatures took " ++ (show $ duration `quot` 10^9) ++ " miliseconds")
    putStrLn ("Tres comas in " ++ show tres_comas ++ " hours")
    now <- getPOSIXTime
    let filename = (show now) ++ ".pallet"
    B.writeFile filename (szPallet pl)
    putStrLn ("PALLET_FILENAME:" ++ filename)
    putStrLn "Done"


octets :: Word32 -> [Word8]  -- found: https://mail.haskell.org/pipermail/beginners/2010-October/005571.html
octets w =
    [ fromIntegral (w `shiftR` 24)
    , fromIntegral (w `shiftR` 16)
    , fromIntegral (w `shiftR` 8)
    , fromIntegral w
    ]


createVote :: IO Vote
createVote = do
    n <- UR.getStdRandom (UR.randomR (0,2^32-1 :: Word32))  -- Word32; using 2^32 overflows to 0 thus n always == 0
    let d = (foldl xor 0 $ octets n) `mod` 2  -- set vote to xor of each byte in Word32 nonce, all modulo 2 (really only LSBs matter)
    return $ Vote d n


mkPallet :: Word32 -> IO Pallet
mkPallet pSize = do
  boxes <- forM [0..pSize - 1] (const mkBox)
  putStrLn ("Generating with " ++ show pSize ++ " boxes")
  return $ Pallet pSize boxes

mkBox :: IO Box
mkBox = do
  vs <- (forM [0 .. batchSize - 1 :: Word32] (const createVote))
  sigs <- genSigsForVotes vs
  return $ Box batchSize vs sigs

genSigsForVotes :: [Vote] -> IO [SigPair]
genSigsForVotes vs = do
  sigs <- sequence $ take (length vs) $ repeat deliverSigPair
  return sigs
  where
   _hash :: ByteString
   _hash = BA.convert $ defaultHash $ szVotes vs
   deliverSigPair :: IO SigPair
   deliverSigPair = do
       g <- getSystemDRG
       let skStr = fst (randomBytesGenerate 32 g) :: ByteString
       let sk = (fromJust $ importPrivate skStr) :: PrivateKey
       let pk = generatePublic sk :: PublicKey
       let sig = sign _hash sk pk :: Signature
       return $ SigPair sig pk


defaultHash :: ByteString -> Digest SHA3_256
defaultHash = hash

--
-- Returns Nothing in two cases
--   1. The ByteString is not in Base58
--   2. The resulting bytes are not `multihashLen` bytes long
--
base58MultihashToByteString :: ByteString -> Maybe ByteString
base58MultihashToByteString mh = do -- Maybe monad
  bytes <- decodeBase58 bitcoinAlphabet mh
  case B.length bytes == multihashLen of
    True  -> return bytes
    False -> Nothing

byteStringToBase58Multihash :: ByteString -> Maybe ByteString
byteStringToBase58Multihash bs =
  case B.length bs == multihashLen of
    True ->  Just $ encodeBase58 bitcoinAlphabet bs
    False -> Nothing

--
-- Given a Base58 encoded multihash (maybe) returns pallet header
-- Header format: <TODO>
--
mkPalletHeader :: PrivateKey -> ByteString -> Maybe ByteString
mkPalletHeader fluxSigningKey base58MultiHash = toHeader <$> mbMultihashBytes
 where
  mbMultihashBytes = base58MultihashToByteString base58MultiHash
  toHeader multihashBytes = header
    where
      versionPlusMultihash = palletHeaderVersion `B.append` multihashBytes
      Sig sig = sign versionPlusMultihash fluxSigningKey (generatePublic fluxSigningKey)
      header = versionPlusMultihash `B.append` sig

data PalletHeaderError =
    IncorrectSignature
  | IncorrectFormat
  deriving (Read, Show, Eq)

--
-- Given a pallet header returns either, the base58 encoded SHA2-256
-- multihash or an error
--
palletHeaderToBase58Multihash :: ByteString
                              -> Either PalletHeaderError ByteString
palletHeaderToBase58Multihash header =
  case mbParts of
    Just [_,multihashBytes,sig] ->
      let multihash = encodeBase58 bitcoinAlphabet multihashBytes
          versionPlusMultihash = palletHeaderVersion `B.append` multihashBytes
      in case valid versionPlusMultihash fluxPublicKey (Sig sig) of
        True  -> Right $ multihash
        False -> Left IncorrectSignature
    _ -> Left IncorrectFormat
  where
    mbParts = partition [B.length palletHeaderVersion, multihashLen, ed25519SigLength] header
    ed25519SigLength = 64  -- bytes

--
-- Takes a list of lengths and a ByteString and partitions it into
-- pieces of those lengths. The lengths must add up to the length of the
-- ByteString or this returns Nothing
--
partition :: [Int] -> ByteString -> Maybe [ByteString]
partition [] _ = error "not defined on empty list"
partition [n] bs =
  case B.length bs == n of
    True  -> Just [bs]
    False -> Nothing
partition (n:ns) bs =
  case B.null bs'' of
    True  -> Nothing
    False -> do -- Maybe monad
      splits <- partition ns bs''
      return (bs':splits)
  where
    (bs', bs'') = B.splitAt n bs
