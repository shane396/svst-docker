    {-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeSynonymInstances       #-}
module SVST.Persist where

import           Data.ByteString
import           Data.Int
import           Database.Persist.TH

-- friends
import           SVST.PersistTypes

--
-- We just use a flat representation.
-- It is true that one transaction can have many null datas but this is
-- rare. For this reason (txId, data) is not unique
-- Additionally, we can just make it part of the spec that txs with multiple
-- nulldatas are invalid... at least for now.
--
share [mkPersist sqlSettings] [persistLowerCase|
  NullData
    prefix              Prefix6
    txId                SHA256
    data                Hex160
    blockTime           Int32
    blockHash           SHA256
    UniqueTxIdAndBlockHash txId blockHash
    deriving (Show)

  Pallet
    nullData                NullDataId
    headerIpfsMultihash     ByteString
    ipfsMultihash           ByteString Maybe
    verified                Bool
|]

