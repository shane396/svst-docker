{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Control.Monad
import System.Environment
import System.Exit
import Text.Read

-- friends
import SVST.Pallet
import SVST.Util

main :: IO ()
main = do
  prog <- getProgName
  args <- getArgs
  exitIfToFewArgs prog args
  case readMaybe (head args) of  -- readMaybe does the magic of converting to Word32 through type inference
    Just nBoxes -> generatePallet nBoxes
    Nothing -> exitWithError "Number of boxes not numeric"
  where
    exitIfToFewArgs prog args = do
      when (length args < 1) $
        exitWithError ("Usage: " ++ prog ++ " <number of boxes>")
