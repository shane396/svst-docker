{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           Control.Concurrent
import           Control.Monad
import           Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import           Data.Maybe
import           Data.Set (Set)
import qualified Data.Set as S
import           Data.Text (Text)
import           Database.Persist
import           Options.Applicative
import           System.Command
import           System.Directory
import           System.IO

-- friends
import           SVST.DBMonad
import           SVST.NullData
import           SVST.Persist
import           SVST.PersistTypes
import           SVST.Pallet
import           SVST.Util

--
-- Download of headers/pallets should occur concurrently with their
-- verification.
--

numDownloadWorkers :: Int
numDownloadWorkers = 1

--------------------------------------------------------------------------------
-- Command line arguments

data Opts =
  Opts {
    optStartBlock     :: BlockIndex -- FIXME: Use this parameter
  , optNulldataPrefix :: ByteString
  } deriving (Read, Show, Eq)

optsParser :: Parser Opts
optsParser =
      Opts
  <$> (read <$> strOption (long "start" <> metavar "BLOCKINDEX" <>
                           value "0" <> help "Start at block BLOCKINDEX"))
  <*> (BC.pack <$> strOption (long "prefix" <> metavar "<nulldata prefix>" <>
                    value (BC.unpack defaultNulldataPrefix) <>
                    help "set SVST nulldata prefix to <nulldata prefix>"))

optsInfo :: ParserInfo Opts
optsInfo =
  info (helper <*> optsParser)
       (fullDesc <> progDesc "A program to download vote data and verify it")

--------------------------------------------------------------------------------

--
-- There are two sets:
--   1. The "to download" set
--   2. The "to verify" set
--
-- The first contains the multihashes of pallet headers
-- The second contains pairs of multihashes of pallet header/pallet files
--
-- The downloader worker(s)
--

beginsWith :: EntityField record Hex160 -> Hex160 -> Filter record
beginsWith field (Hex160 val) =
  Filter field (Left $ Hex160 $ B.concat [val,"%"])
               (BackendSpecificFilter "like")
--  where
--    ap (Hex160 bs) bs' = Hex160 (bs `B.append` bs')

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  opts <- execParser optsInfo
  let pfx = optNulldataPrefix opts
  results <- withDBM $ selectList
               [NullDataData `beginsWith` Hex160 pfx] []
  let multihashes = catMaybes $ map (toMultihash pfx . nullDataData . entityVal) results
--   mapM_ processPallet multihashes
  let n = length multihashes
  dlMVar     <- newMVar (S.fromList multihashes)
  verifyMVar <- newMVar S.empty
  countMVar  <- newMVar 0
  replicateM_ numDownloadWorkers (forkIO $ downloadWorker dlMVar verifyMVar)
  forkIO $ verifyWorker verifyMVar countMVar
  waitForCompletion countMVar n
  putStrLn "All pallets verified"
  return ()

waitForCompletion :: MVar Int -> Int -> IO ()
waitForCompletion mvar n = do
  n' <- readMVar mvar
  case n == n' of
    True  -> return ()
    False -> threadDelayMillisecs 1 >> waitForCompletion mvar n


runCmd :: FilePath -> [String] -> IO (Either String ())
runCmd path args = do
  (_, stdoutH, stderrH, procH) <- runInteractiveProcess path args Nothing Nothing
  exitCode <- waitForProcAndStreamOutput procH stdoutH stderrH
  case isSuccess exitCode of
    True  -> return $ Right ()
    False -> hGetContents stderrH >>= return . Left
  where
    --
    -- Streams all output from stdoutH to stdout and stderrH to stderr
    --
    waitForProcAndStreamOutput :: ProcessHandle -> Handle -> Handle -> IO ExitCode
    waitForProcAndStreamOutput procH stdoutH stderrH = go
      where
        go = do
          hRedirectAvailable stdoutH stdout
          hRedirectAvailable stderrH stderr
          mbCode <- getProcessExitCode procH
          case mbCode of
            Just code -> return code
            Nothing -> go
--
-- FIXME:
-- Things that could go wrong. Handle all cases and warn
-- 0. nulldata is malformed and doesn't contain valid multihash
-- 1. ipfs daemon is not running
-- 2. Pallet header does not exist
-- 3. Pallet header is malformed
-- 4. Pallet in pallet header does not exist
--
downloadWorker :: MVar (Set ByteString)
               -> MVar (Set ByteString)
               -> IO ()
downloadWorker dlMVar verifyMVar = loopWithMillisecondDelay 1 go
  where
    go = do
      s <- takeMVar dlMVar
      if S.null s
       then putMVar dlMVar s
       else do
         putMVar dlMVar $ S.deleteMin s
         -- FIXME: What happens if this fails?
         downloadPallet $ S.findMin s
         threadDelayMillisecs 1
    ipfsGet :: ByteString -> IO (Either String ())
    ipfsGet mh = do
      waitForIPFSDaemon
      let args = ["get", mh', "-o", "/tmp/" ++ mh' ]
          mh' = BC.unpack mh
      runCmd "ipfs" args
    downloadPallet :: ByteString -> IO ()
    downloadPallet multihash = do
      putStrLn $ "Download pallet header " ++ mh
      res <- ipfsGet multihash
      let headerFile = "/tmp/" ++ mh
      mhExist <- doesFileExist headerFile
      when (mhExist && res == Right ()) $ do
        header <- B.readFile headerFile
        case palletHeaderToBase58Multihash header of
          Right palletMultihash -> do
            res <- ipfsGet palletMultihash
            -- FIXME: Check for existence of file
            fileExist <- doesFileExist $ "/tmp/" ++ BC.unpack palletMultihash
            when (fileExist && res == Right ()) $ do
              modifyMVar_ verifyMVar $ \s -> return $ S.insert palletMultihash s
          Left err -> do
            putStrLn $ show err -- FIXME: Will just keep displaying error
        where
          mh :: String
          mh = BC.unpack multihash

verifyWorker :: MVar (Set ByteString) -> MVar Int -> IO ()
verifyWorker verifyMVar countMVar = loopWithMillisecondDelay 1 go
  where
    go :: IO ()
    go = do
      s <- takeMVar verifyMVar
      if S.null s
       then putMVar verifyMVar s
       else do
         let palletHash = S.findMin s
             pmh = BC.unpack palletHash
         -- FIXME: What if failure?
         putMVar verifyMVar $ S.deleteMin s
         verifyPallet ("/tmp/" ++ pmh)
         modifyMVar_ countMVar $ \n -> return $ n + 1

--
-- FIXME: Update in database status of pallet verification
--
verifyPallet :: FilePath -> IO ()
verifyPallet palletFile = do
  putStrLn "Verifying pallet..."
  (exitCode, _, _) <-
    readProcessWithExitCode "/svst-data/verify-pallet" [palletFile] ""
  case (isSuccess exitCode) of
    True -> putStrLn "Pallet successfully verified!\n"
    False -> putStrLn "Pallet didn't verify\n"

--
-- Waits forever for IPFS daemon to come online
--
waitForIPFSDaemon :: IO ()
waitForIPFSDaemon = do
  retryForever checkForIPFS
  where
    checkForIPFS :: IO (Either Text ())
    checkForIPFS = do
      (ec, _, _) <- readProcessWithExitCode "echo" [{-"swarm", "peers"-}] ""
      return $ case isSuccess ec of
        True  -> Right ()
        False -> Left "IPFS daemon is not running"


toMultihash :: ByteString -> Hex160 -> Maybe ByteString
toMultihash prefix (Hex160 bs) = byteStringToBase58Multihash bytes
  where
    bytes = B.drop (B.length prefix) bs