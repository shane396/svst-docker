#!/usr/bin/env bash

set -e

function get_sut_container_id {
    docker ps | grep "ci_sut" | cut -d ' ' -f 1
}

until [ ! -z $(get_sut_container_id) ]; do
    echo "### waiting for SUT container ..."
    echo $(get_sut_container_id)
    sleep 10
done
echo "$$$ System Under Test (SUT) UP... executing docker wait"

docker ps

goodtest=$(docker wait $(get_sut_container_id))  # wait for SUT container to finish

./_cleanup_tests.sh

echo "Cleaning dangling images"
docker rmi $(docker images -q -f "dangling=true") > /dev/null || true
docker volume rm $(docker volume ls -f dangling=true -q) || true


echo "Test returned $goodtest"
exit "$goodtest"
