# Secure Vote Stress Test

This respository manages all docker, python, c, and haskell files for the Secure Vote Stress Test.

If you'd like the docs expanded please raise an issue.

The stress test is scheduled for 1488812400 (which is Mon, 06 Mar 2017 16:00:00 GMT or Tues, 7 Mar 2017 02:00:00 ADST)

## Requirements:

* You will need a new version of `docker-compose` and `docker`.
* You should also have 500GB of space to use (though you shouldn't _need_ more than 400 GB).
* This system expects Ubuntu 16.04 ONLY - no other OSs are supported yet.
* At _least_ 24 hrs setup time before the stress test as Bitcoin needs to sync with the network. It might be easier if you can copy your `.bitcoin` folder from elsewhere.
* At _least_ 1.5 MB/s down consistently

Tested with AWS and Digital Ocean nodes (though we are only using AWS atm as we found them more reliable performance wise).

## Recommended Setup

We test this on `c4.xlarge` and `m4.xlarge` machines on AWS. `t2.xlarge` underperformed by a large margin.

This is a very CPU intensive stress test.
Virtual servers will run hot all day, though some high performance desktops or laptops will be okay.

## I just want to run it!

**WARNING** - This will _not_ respect your existing docker containers. If you do want to run this locally run it in a VM! - **WARNING**

Requirements: have a remote server with Ubuntu 16.04 on it and the ability to SSH in (best via a key stored in `.ssh/id_rsa.pub`, etc).

* Clone the repository (https://gitlab.com/exo-one/svst-docker) to your machine.
* If you don't want to bootstrap the blockchain download: 
  * From `svst-docker/` run `./mainnet/0_setup_remote.sh <USER> <REMOTE>` from `./svst-docker` making sure to put in the 
  right username and hostname/IP. (Use bash on windows maybe if you're a windows user)
  * If you do want to bootstrap
    * First run `./mainnet/l1_setup_repos.sh USER REMOTE`
    * Then copy `.bitcoin` to `svst-docker/bitcoind-data/.bitcoin` as mentioned in the bootstrap section below
    * Then run `./mainnet/x_init.sh USER REMOTE` to start the containers
* This will SSH into a machine, install the requirements, build the containers, and start the stress test. 
  Tested on Ubuntu 16.10 and macOS 10.12.3

All scripts are intended to be run from the repository's root directory.

## Vote Explorer

We have a very basic vote-explorer dashboard available on port 8683 ("vote" on an old mobile keypad).
There are two main files: `index.html` and `debug.html`, you'll be directed to `index.html` by default.

## Basic Troubleshooting

If you get stuck try running `./2_refresh_all.sh` on the stress-test box which will redownload
all base images and rebuild all containers.

### How do _you_ run it?

We (at XO.1) will run it in 'producer' mode - part of this involves providing the secret key used to sign headers.
Headers are only signed during the stress test as an anti-DoS measure, SecureVote itself will not use signed headers
(this would introduce a central authority).

## Codebase Organisation

The intended structure _for the multiple repositories_ is:

```
└── svst-docker
    ├── bitcoin-nulldata
    ├── stress-test-pallet-verification
    ├── svst-docs
    ├── svst-haskell
    └── svst-python
```

This allows the docker setup to copy the local files into Dockerfiles.

## Base Images

There are some base images we build other containers from. The main benefit is a common setup and caching.

* `dev-base-1` - this is the general base image, meant to include as many dev resources as possible
* `dev-base-haskell` - this is the base image for Haskell utils, based on dev-base-1. It has an out of date version of `svst-haskell`, however, stack has been upgraded and most libraries have cached prebuilt versions, so it's much faster to build on top of (rather than recompiling the same files over and over)
* `dev-base-python` - this is like the haskell image above and also based on dev-base-1. Since python doesn't need to compile much and a pip install is fast it is just an interim layer for convenience
* `dev-test-bitcoind` - a testing image used to skip over compiling bitcoind; not to be used in production

## Microservice images

In the `dockerfiles` subdirectory there are a number of folders with corresponding docker files.
These are responsible for building and running each individual service.

There are also a number of dockerfiles in the main directory.

Our microservices are:

* `bitcoind` (Bitcoin-Nulldata)
* `ipfs` (decentralised content network)
* `postgres` (database, for data-base-y things)
* `producer` (for producing nulldata, headers, and pallets)
* `scraper` (for scraping nulldata)
* `header-download` (for downloading and verifying pallet headers)
* `pallet-download` (for downloading pallets from verified headers)
* `pallet-verifier` (for verifying the signatures of pallets and the pallets themselves)
* `state-maker` (for calculating the state - essentially running through all valid pallets to count votes)
* `vote-explorer` (a simple html interface to explore votes and show the state of the network)

These are all started through the `docker-compose.yaml` files

## Volumes

We use volumes to make the docker images able to be used through all stages of the apps lifetime. All _state_ should be
stored in these folders, essentially.

* `bitcoind` uses `./bitcoind-data` to store `.bitcoin/` including configuration files and the main blockchain
* `ipfs` uses `./ipfs-data` to store _all_ downloaded pallets and headers (except invalid ones)
  * keep in mind, all IPFS chunks are stored here and the other services only download them via the API temporarily to
  verify / process them.
* `postgres` uses `./postgres-data` to store the DB files

### Bootstrapping off existing Bitcoind data

If you have an existing bitcoin full node you can use it's blockchain data instead of downloading it from scratch (phew).
You will need to make sure `bitcoind` is shut down before copying any DB files, and you will want to make sure you don't
copy your wallet.dat if you use one.

**WARNING** - You will need to delete your previous `bitcoin.conf`. The stress test will set up a new one with a known
RPC password automatically. Please also ensure you don't copy your wallet.dat over.

Copy `~/.bitcoin` to `svst-docker/bitcoind-data/.bitcoin` and it should be picked up
by the bitcoin docker container. You should do this before you bring the containers up.

Final directory structure should look like:

```
svst-docker/
├── bitcoind-data/
│   ├── bitcoind-data  # just an empty file
│   └── .bitcoin/
│       ├── bitcoin.conf
│       ├── blocks/
│       ├── chainstate/
        └── other files...
(more files)
```

### Building Individual Images

Upon occasion you will need to rebuild a single image after changes have been
made upstream.

You will need to supply docker-compose with the instruction to build, to not
use the existing cached image as well as which image needs rebuilding:

    % docker-compose build --no-cache image_name

ie: rebuilding scraper:

    % docker-compose build --no-cache scraper

You can use `_rebuild_ci_container.sh` if you'd like to rebuild on for the testing framework.
Alternatively you can also run `_reset_tests.sh`

## Testing

Testing done through `docker-compose`'s testing framework.

See `.gitlab-ci.yml` for latest testing entrypoint and setup (in `run-tests`)

You can run tests manually with `_run_tests.sh` or `_dev_run_test.sh container1 container2 ...` to rebuild only some specific containers.

If you need to rebuild all containers for CI run `_reset_tests.sh` - e.g. if you alter svst-haskell and need to re-compile

Use these scripts for various testing features:

* `_reset_tests.sh` - rebuild all test containers
* `_rebuild_ci_container.sh` - rebuild a single container (useful for testing a single container quickly)
* `_debug_btc` - run bitcoin-cli commands quickly
* `_cleanup_tests.sh` - stops all containers from CI

## Testnet

The testnet mode is operated through the `./testnet/` directory and mirrors the mainnet commands.

They're all designed to operate on remote machines.

Briefly these are the interesting commands:
* `./testnet/r0_initialize_remote.sh USER REMOTE` - this will just call `r1` and `x_testnet_init`
* `./testnet/r1_setup_repos.sh USER REMOTE`
* `./testnet/x_testnet_init.sh USER REMOTE`

If you're operating locally it's best to have a read of the above scripts, but then to use `r2_auditor.sh`

# Notes

As it turns out: the `.dockerignore` file is really important if you have any data in bitcoind-data or ipfs-data, etc.
If you don't have one your machine will transfer many 10s of GB to docker each build.

