#!/usr/bin/env bash

function sshdown {
    ssh "$1@$2" "cd src/svst/svst-docker; ./testnet/btc stop; ./testnet/dc down --rmi local --remove-orphans"
}

sshdown "$1" "$2"
