#!/usr/bin/env bash

if [ $# -ne 2 ]; then
    echo "useage: ./testnet/r0_initialize_remote.sh USER HOST"
    echo "Requirement: Need to be able to login via ssh to USER@HOST"
    echo "This script will then automatically configure the box to work for XO.1's secure.vote stress test."
    exit 1
fi

./testnet/r1_setup_repos.sh "$1" "$2"
./testnet/x_testnet_init.sh "$1" "$2"
