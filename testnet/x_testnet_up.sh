#!/usr/bin/env bash

function sshcmd {
    ssh "$1@$2" "cd src/svst/svst-docker && tmux new-session -d './testnet/dc up | tee -a log-full'"
}

sshcmd "$1" "$2"
