#!/usr/bin/env bash

function sshcmd {
    ssh "$1@$2" "cd src/svst/svst-docker; ./testnet/btc stop; ./testnet/dc stop -t 20"
}

sshcmd "$1" "$2"
