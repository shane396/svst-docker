#!/bin/bash

echo "Cleaning dangling images"
docker rm -v $(docker ps -a -q -f status=exited) > /dev/null || true
docker rmi $(docker images -f "dangling=true" -q) > /dev/null || true
docker volume rm $(docker volume ls -qf dangling=true) > /dev/null || true

# pull images first since we build in parallel
images=("exo1/dev-base-1" "exo1/dev-base-haskell" "exo1/dev-base-python" "exo1/dev-test-bitcoind")
for image in ${images[@]}; do
    docker pull "$image"  # NB: Don't pull in parallel since they depend on one another
done

containers=(header-download pallet-download producer scraper pallet-validator state-maker vote-explorer postgres)
for container in ${containers[@]}; do
    ./testnet/dc build --no-cache $container &
done
wait

./testnet/dc up
