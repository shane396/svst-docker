Title: PSA: We're running a stress test of our voting system, and there might be some congestion.

# What's Happening

At 1488812400 UTC (06 Mar 2017 16:00:00 UTC, or 2am on the 7th in Australia) 
[XO.1](https://xo1.io) and [Flux](https://voteflux.org) will be demonstrating the
throughput of the [SecureVote](https://secure.vote) voting system. Over 1 million 
votes per minute will be anchored to the Bitcoin blockchain, for a 
total of approximately 1.5 billion votes over the 24 hours.

This will take place through approximately 11,000 transactions over 24 hours. This is about 4% of Bitcoin's capacity
so may cause increased confirmation times and/or fees.

Source code and documentation can be found at: https://gitlab.com/exo-one/svst-docker and we have a [white-knapkin](https://gitlab.com/exo-one/svst-docker/blob/master/svst-docs/secure.vote.white.knapkin.md)
if you'd like some more technical data.

You're welcome to run an auditing node to verify the throughput of this stress test for yourself. (This can also be done after the stress test has concluded)

# What is SecureVote

SecureVote is a product by XO.1 - the world's first high capacity, secure, general purpose, end to end verifiable voting system.
There is no central point of failure, all votes are fully anonymized, and no fancy new crypto is required (IE: not ZKPs or homomorphic encryption).

# What is XO.1

XO.1 is a small startup (currently in the early stages of our next fundraising round) founded by myself (Max Kaye) and Nathan Spataro.
XO.1 was created to provide a commercially viable secure voting system, something which does not exist anywhere on the planet today.
A key requirement for us is high throughput, something that requires a second-layer solution.

# What is Flux

[Flux](https://voteflux.org) is a political movement also founded by myself and Nathan Spataro.
The Flux Movement aims to instantiate a novel form of democracy called Issue Based Direct Democracy (IBDD).
Unlike all other forms of democracy, IBDD is deeply concerned with epistemology and providing the best decision
making possible.

IBDD is designed to bias _objectivley true knowledge_ and is possibly the first democratic [truth machine](http://www.philosophersbeard.org/2010/11/democracy-is-not-truth-machine.html).

We hold that the main problem with democracy today is that it does not bias the best knowledge possible. The 
symptoms of this are corruption, bloat, sluggishness, low approval ratings, etc.

If that sort of thing interests you, I have a pre-release paper I can send on request (but not yet publish). Also, 
the philosophical groundwork is laid out in David Deutsch's breathtakingly profound book *[The Beginning of Infinity](https://en.wikipedia.org/wiki/The_Beginning_of_Infinity)* (2011).

We're expanding with chapters globally so if you'd like to get involved please get in touch.

## Why don't we use Ethereum

I've been involved in the Ethereum world since [December 2013](https://web.archive.org/web/20140208053651/https://bitcointalk.org/index.php?topic=428589.0)
and have been [publically credited](https://youtu.be/o6D8Up411dI?t=29m16s) a [few times](http://imgur.com/piL9eYc)
with contributing to scaling discussions around that sort of technology.

My view boils down to this: Ethereum can do _anything_, but it can never do _everything_.

This stress test will produce about 150 GB of data. This isn't something the main Ethereum chain can handle, and even
many years into the future it would be selfish to dump this volume of data into a shared resource.

Additionally, relying on others to eventually come up with a scaling solution isn't a great strategy IMO.

Since we needed a 2nd layer solution anyway it only makes sense to build on the most secure blockchain around: Bitcoin.

Additionally: _secure_ private blockchains are a pipe dream. Private blockchains themselves are both easy and pointless (when it comes to public security).

## Footnotes:

1. Secure here means a lot of things: no central point of failure, able to be run without the _active_ participation of
a central authority, secret ballot, fully cryptographically verifiable by all parties, using battle tested crypto only, etc.

